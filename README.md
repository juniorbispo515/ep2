# EP2 - OO 2019.1 (UnB - Gama)

##

##Diagrama de Classe
    https://imgur.com/a/ibz4XRG

## Problema

A xDese, que possui diversos tipos de veículos e transporta diversas cargas, foi a última empresa na qual Zé do Caminhão trabalhou. Zé era o funcionário mais antigo e a sua tarefa era decidir qual o melhor veículo para o transporte de uma determinada carga. Dentre os veículos disponíveis, Zé era capaz de decidir, de acordo com a carga e a distância a ser percorrida, qual era a opção que resultava no maior lucro. Além disso, caso não fosse lucrativa, a entrega poderia ser recusada.

A tarefa não é trivial, pois leva em consideração algumas peculiaridades dos veículos disponíveis:
- Carreta
  - **Combustível**: Diesel
  - **Rendimento**: 8 Km/L
  - **Carga máxima**: 30 toneladas
  - **Velocidade média**: 60 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.0002 Km/L
- Van
  - **Combustível**: Diesel
  - **Rendimento**: 10 Km/L
  - **Carga máxima**: 3,5 toneladas
  - **Velocidade média**: 80 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.001 Km/L
- Carro
  - **Combustível**: Gasolina ou Álcool
  - **Rendimento**: 14 Km/L com gasolina, 12Km/L com álcool
  - **Carga máxima**: 360 Kg
  - **Velocidade média**: 100 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.025 Km/L com gasolina e 0.0231 Km/L com álcool
- Moto
  - **Combustível**: Gasolina ou Álcool
  - **Rendimento**: 50 Km/L com gasolina, 43 Km/L com álcool
  - **Carga máxima**: 50 kg
  - **Velocidade média**: 110 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.3 Km/L com gasolina e 0.4 Km/L com álcool


  Os valores dos combustíveis a serem utilizados são:
  - **Álcool**: R$ 3.499 por litro
  - **Gasolina**: R$ 4.449 por litro
  - **Diesel**: R$ 3.869 por litro

## O Programa:
 Esse é a primeira tela que o usuário irá encontrar:
 <p align="center">
 <img src= "https://i.imgur.com/LLtNuFp.png">
 </p>
 
## Proposta:
  Três campos aparecerão pra colocar os dados da entrega:
  - **Distância**: Dada em km.
  - **Carga**: Dada em Kg.
  - **Tempo**: Dada em horas.
  <p align ="center">
  <img src="https://i.imgur.com/00fUy3k.png">
  </p>

## Calculo:
  Quando três dados válidos forem inseridos irá pra aba que mostra:
  - **Melhor Custo**
  - **Melhor Tempo**
  - **Melhor Custo/Benefício**: O menor produto entre custo e tempo.
  <p align ="center">
  <img src="https://i.imgur.com/15p4ACA.png">
  </p>
  
  Onde o "Valor Minimo" é o custo da operação somada a margem de lucro (custo*(1 + margem/100)).

  E o usuario poderá escolher a opção que mais agrada e assim o devido veículo será alocado ou voltar para tela principal.

## Margem:
  Para inserir uma nova margem de lucro:
  <p align ="center">
  <img src="https://i.imgur.com/B9Ns1mr.png">
  </p>

## Frota:
   Ver sua frota e administrar ela:
   <p align ="center">
   <img src ="https://i.imgur.com/mzkLt93.png">
   </p>
   


