/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.FormatterClosedException;
import java.lang.SecurityException;
import java.io.*;

public class Arquivo {
    
    private Formatter arquivo;
    
    public void abrir(String nome){
    try{
        arquivo = new Formatter(nome);
    }
    
    catch(SecurityException|FileNotFoundException e){
        System.exit(1);
    }
    
    }


    public void escrever(String nome){
        try{
            arquivo.format(nome);
        }
        catch(FormatterClosedException|NoSuchElementException e){
            System.exit(1);
        }
    
    }
    
    public void fechar(){
        arquivo.close();
    }
    
    public String[] pegaOcupados(){
            String[] ocupados = new String[4];
        try{
                    FileInputStream entrada = new FileInputStream("frota.txt");
                    InputStreamReader entradaFormatada = new InputStreamReader(entrada);
                    BufferedReader entradaString = new BufferedReader(entradaFormatada);
        
                   
        
                   String s= entradaString.readLine();
                   
                   s = entradaString.readLine();
                   ocupados = s.split(" ");
                   return ocupados;
                }
                
                catch(FileNotFoundException e1){
                    e1.printStackTrace();
                }
                
                catch(IOException e){
                    e.printStackTrace();
                }
                return ocupados;
    }
    
    public String[] pegaDesocupados(){
            String[] desocupados = new String[4];
        try{
                    FileInputStream entrada = new FileInputStream("frota.txt");
                    InputStreamReader entradaFormatada = new InputStreamReader(entrada);
                    BufferedReader entradaString = new BufferedReader(entradaFormatada);
        
                   
        
                   String s= entradaString.readLine();
                   
                   desocupados = s.split(" ");
                   return desocupados;
                }
                
                catch(FileNotFoundException e1){
                    e1.printStackTrace();
                }
                
                catch(IOException e){
                    e.printStackTrace();
                }
                return desocupados;
    }
    
    public static String pegaMargem(){
        try{
                    FileInputStream entrada = new FileInputStream("Margem.txt");
                    InputStreamReader entradaFormatada = new InputStreamReader(entrada);
                    BufferedReader entradaString = new BufferedReader(entradaFormatada);
        
                   
        
                   String s= entradaString.readLine();
                   return s;
                }
                
                catch(FileNotFoundException e1){
                    e1.printStackTrace();
                }
                
                catch(IOException e){
                    e.printStackTrace();
                }
                return " ";
    }
    
        
    }

