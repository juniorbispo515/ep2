package Classes;

import Classes.Veiculo;


public class Moto extends Veiculo{
        
    private final int rendimento_2;
    private final double decrescimo_2;
    public Moto(){
        super(110, 50, 50, 0.3, "Moto");
        this.rendimento_2 = 43;
        this.decrescimo_2 = 0.4;
}
    
@Override
    public double calculaValor(double carga,double distancia, double tempo){

        double valorFinal, valorAlcool, valorGasolina;
        
        valorAlcool = (distancia/(rendimento_2 - decrescimo_2 * carga)) * 3.499;
        valorGasolina = (distancia / (getRendimento()- getDecrescimo() * carga)) * 4.449;
        
        if(valorGasolina > valorAlcool) return valorAlcool;
        else return valorGasolina;
    }

//Sobrecarga da classe Veiculos
    public String verCombustivel(double carga,double distancia){
        if ((distancia/(getRendimento() - getDecrescimo()*carga ))*4.449 < (distancia/(rendimento_2 - decrescimo_2 * carga))*3.499) return "gasolina";
        else return "alcool";
    }

@Override
    public boolean possivel(double carga, double distancia, double tempo){
       String desocupados[] = new String[4];
       Arquivo aqv = new Arquivo();
       desocupados = aqv.pegaDesocupados();
       if(carga > getLimite() || distancia > getVelocidade()*tempo || desocupados[3].equals("0")) return false;
       return true;
    }
};
