package Classes;

import Classes.Veiculo;


public class Van extends Veiculo{
    
    public Van(){
        super(80, 10, 3500, 0.001, "Van");
    }

@Override
    public boolean possivel(double carga, double distancia, double tempo){
       String desocupados[] = new String[4];
       Arquivo aqv = new Arquivo();
       desocupados = aqv.pegaDesocupados();
       if(carga > getLimite() || distancia > getVelocidade()*tempo || desocupados[1].equals("0")) return false;
       return true;
    }
};
