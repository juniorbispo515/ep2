// Criação da classe abstrata veiculo.

package Classes;
    

public abstract class Veiculo {
    
    private final double rendimento;
    private final int limite;
    private final int velocidade;
    private final double decrescimo;
    private final String nome;
    /*
      A ideia é cada Veiculo seja igual ao outro logo não precisaria de um identificador e etc.
      Não teria muito propósito em colocar isso pois seria mera miscelânea.
    */
    
    public Veiculo(int velocidade, double rendimento, int limite, double decrescimo,String nome){
        this.rendimento = rendimento;
        this.limite = limite;
        this.velocidade = velocidade;
        this.decrescimo = decrescimo;
        this.nome = nome;
    }
    
    public double getRendimento() {
        return rendimento;
    }

    public int getLimite() {
        return limite;
    }

    public int getVelocidade() {
        return velocidade;
    }
    
    public double getDecrescimo(){
        return decrescimo;
    }
    
    public String getNome(){
        return nome;
    }
    
    public double calculaValor(double carga,double distancia, double tempo){
         double valorFinal;
    
         valorFinal = (distancia/(getRendimento()- decrescimo*carga))*3.869; // Valor da Entrega em si
    
         return valorFinal;
    }
 

    public double tempoEntrega(double distancia, double tempo){
        
        return distancia/velocidade; 
    }

    
    public String verCombustivel(double carga,double distancia){
       return "diesel";
    }


    public boolean possivel(double carga, double distancia, double tempo){
       if(carga > getLimite() || distancia > getVelocidade()*tempo) return false;
       return true;
    }
};
    