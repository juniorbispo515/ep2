package Classes;

import Classes.Veiculo;
import Classes.Arquivo;

public class Carreta extends Veiculo{
    
    public Carreta(){
       super(60, 8, 30000, 0.0002, "Carreta");
    }
    
@Override    
    public boolean possivel(double carga, double distancia, double tempo){
       String desocupados[] = new String[4];
       Arquivo aqv = new Arquivo();
       desocupados = aqv.pegaDesocupados();
       if(carga > getLimite() || distancia > getVelocidade()*tempo || desocupados[0].equals("0")) return false;
       return true;
    }
};
