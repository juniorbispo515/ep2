package Classes;
import Classes.Veiculo;


public class Carro extends Veiculo{
    
    private final int rendimento_2;
    private final double decrescimo_2;
    
    public Carro(){
        super(100, 14, 360, 0.025, "Carro");
        this.rendimento_2 = 12;
        this.decrescimo_2 = 0.0231;
    }
    
@Override    
    public double calculaValor(double carga,double distancia, double tempo){

        double valorFinal, valorAlcool, valorGasolina;
        
        valorAlcool = (distancia/(rendimento_2 - decrescimo_2 * carga)) * 3.499;
        valorGasolina = (distancia / (getRendimento()- getDecrescimo() * carga)) * 4.449;
        
        if(valorGasolina > valorAlcool) return valorAlcool;
        else return valorGasolina;
    }

//Sobrecarga
    public String verCombustivel(double carga,double distancia){
        if ((distancia/(getRendimento() - getDecrescimo()*carga ))*4.449 < (distancia/(rendimento_2 - decrescimo_2 * carga))*3.499) return "gasolina";
        else return "alcool";
    }

@Override
    public boolean possivel(double carga, double distancia, double tempo){
       String desocupados[] = new String[4];
       Arquivo aqv = new Arquivo();
       desocupados = aqv.pegaDesocupados();
       if(carga > getLimite() || distancia >= getVelocidade()*tempo || desocupados[2].equals("0")) return false;
       return true;
    }
}
