/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Classes.Carreta;
import Classes.Carro;
import Classes.Moto;
import Classes.Van;
import Classes.Veiculo;
import Main.Principal;
import java.text.DecimalFormat;
import Classes.Arquivo;
import javax.swing.JOptionPane;

/**
 *
 * @author Jr
 */
public class Calculo extends javax.swing.JFrame {
    
    public int mc;
    public int mt;
    public int mcb;
    
    static int pegaMenor(double v[]){
        int menor=0;
        double valorMenor=1000000;
        
        for(int i=0;i<4;i++){
            if(v[i]<valorMenor && v[i]>=0){
                menor=i;
                valorMenor=v[i];
            }
        }
        
        return menor;
    }
    
    public Calculo(String distancia, String carga, String tempo, boolean[] possivel) {
        initComponents();
        
        DecimalFormat df2 = new DecimalFormat("#.###");
        Veiculo v[] =  new Veiculo[4];
    
        v[3]= new Moto();
        v[2]= new Carro();
        v[1]= new Van();
        v[0]= new Carreta();
  
        double[] c = new double[4];
        double[] t = new double[4];
        double[] ct = new double[4];
    
    for(int i=0;i<4;i++)
        if(possivel[i]){
        c[i]=v[i].calculaValor(Double.parseDouble(carga), Double.parseDouble(distancia), Double.parseDouble(tempo));
        t[i]=v[i].tempoEntrega(Double.parseDouble(distancia), Double.parseDouble(tempo));
        ct[i]=c[i]*t[i];
    }
    else {
        c[i]=-1;
        t[i]=-1;
        ct[i]=-1;
        
    }
    
   mc = pegaMenor(c); // menorCusto
   mt = pegaMenor(t); // menorTempo
   mcb = pegaMenor(ct);
   String minimo;
   
        lblMelhorCustoCombustivel.setText(v[mc].getNome() + " usando " + v[mc].verCombustivel(Double.parseDouble(carga), Double.parseDouble(distancia)));
        lblMelhorCustoValor.setText("Com Custo de " + df2.format(c[mc]) + " reais");
        lblMelhorCustoTempo.setText("No tempo de " + df2.format(t[mc]) + " hora(s)");
        minimo= df2.format( c[mc] * (1 + Double.parseDouble(Arquivo.pegaMargem()) /100 ));
        lblMelhorCustoProposta.setText("Valor mínimo: " + minimo);
                
        lblMelhorTempoCombustivel.setText(v[mt].getNome() + " Usando " + v[mt].verCombustivel(Double.parseDouble(carga), Double.parseDouble(distancia)));
        lblMelhorTempoValor.setText("Com custo de " + df2.format(c[mt]) + " reais");
        lblMelhorTempoTempo.setText("No tempo de " + df2.format(t[mt]) + " hora(s)");
        minimo= df2.format( c[mt] * (1 + Double.parseDouble(Arquivo.pegaMargem()) /100 ));
        lblMelhorTempoProposta.setText("Valor mínimo: " + minimo);
        
        lblMelhorCBCombustivel.setText(v[mcb].getNome() + " Usando " + v[mcb].verCombustivel(Double.parseDouble(carga), Double.parseDouble(distancia)));
        lblMelhorCBValor.setText("Com custo de " + df2.format(c[mcb]) + " reais");
        lblMelhorCBTempo.setText("No tempo de " + df2.format(t[mcb]) + " hora(s)");
        minimo= df2.format( c[mcb] * (1 + Double.parseDouble(Arquivo.pegaMargem()) /100 ));
        lblMelhorCBProposta.setText("Valor mínimo: " + minimo);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        lblMelhorCustoCombustivel = new javax.swing.JLabel();
        lblMelhorTempoCombustivel = new javax.swing.JLabel();
        lblMelhorCustoValor = new javax.swing.JLabel();
        lblMelhorCustoTempo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lblMelhorTempoValor = new javax.swing.JLabel();
        lblMelhorTempoTempo = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        lblMelhorCustoProposta = new javax.swing.JLabel();
        lblMelhorTempoProposta = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblMelhorCBCombustivel = new javax.swing.JLabel();
        lblMelhorCBValor = new javax.swing.JLabel();
        lblMelhorCBTempo = new javax.swing.JLabel();
        lblMelhorCBProposta = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Melhor Custo");
        jButton1.setBorder(null);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Melhor Tempo");
        jButton2.setBorder(null);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Voltar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setBackground(new java.awt.Color(255, 255, 255));
        jButton4.setText("Melho C/B");
        jButton4.setBorder(null);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton4)
                                    .addComponent(jButton2))
                                .addGap(36, 36, 36)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(lblMelhorCBProposta)
                                    .addComponent(lblMelhorCBTempo)
                                    .addComponent(lblMelhorCBValor)
                                    .addComponent(lblMelhorCBCombustivel)
                                    .addComponent(lblMelhorTempoProposta)
                                    .addComponent(lblMelhorTempoTempo)
                                    .addComponent(lblMelhorTempoValor)
                                    .addComponent(lblMelhorTempoCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblMelhorCustoTempo)
                                    .addComponent(lblMelhorCustoValor, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                                    .addComponent(lblMelhorCustoCombustivel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblMelhorCustoProposta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jButton3))
                        .addContainerGap(19, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jSeparator2)
                        .addGap(10, 10, 10))
                    .addComponent(jSeparator1)))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton1, jButton2, jButton4});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblMelhorCBCombustivel, lblMelhorCBProposta, lblMelhorCBTempo, lblMelhorCBValor, lblMelhorCustoTempo, lblMelhorTempoCombustivel, lblMelhorTempoProposta, lblMelhorTempoTempo, lblMelhorTempoValor});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMelhorCustoCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorCustoValor, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorCustoTempo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorCustoProposta, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblMelhorTempoCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorTempoValor, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorTempoTempo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorTempoProposta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblMelhorCBCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorCBValor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorCBTempo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMelhorCBProposta)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addContainerGap(13, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButton1, jButton2, jButton4, lblMelhorCBProposta, lblMelhorCBTempo, lblMelhorCBValor, lblMelhorCustoCombustivel, lblMelhorCustoTempo, lblMelhorCustoValor, lblMelhorTempoCombustivel, lblMelhorTempoProposta, lblMelhorTempoTempo, lblMelhorTempoValor});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        new Principal().setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
              
        Arquivo arquivo = new Arquivo();
        String frota= "";
        String[] desocupado = new String[4];
        String[] ocupado = new String[4];
        
        ocupado = arquivo.pegaOcupados();
        desocupado = arquivo.pegaDesocupados();
        
        desocupado[mc] = Integer.toString(Integer.parseInt(desocupado[mc])-1);
        ocupado[mc] = Integer.toString(Integer.parseInt(ocupado[mc])+1);
        
        frota+= desocupado[0] + " " + desocupado[1] + " " + desocupado[2] + " " + desocupado[3] + "\n";
        frota+=ocupado[0] + " " + ocupado[1] + " " + ocupado[2] + " " + ocupado[3] ;
        arquivo.abrir("Frota.txt");
        arquivo.escrever(frota);
        arquivo.fechar();
        
        JOptionPane.showMessageDialog(null, "Concluido com sucesso!");
        new Principal().setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Arquivo arquivo = new Arquivo();
        String frota= "";
        String[] desocupado = new String[4];
        String[] ocupado = new String[4];
        
        ocupado = arquivo.pegaOcupados();
        desocupado = arquivo.pegaDesocupados();
        
        desocupado[mt] = Integer.toString(Integer.parseInt(desocupado[mt])-1);
        ocupado[mt] = Integer.toString(Integer.parseInt(ocupado[mt])+1);
        
        frota+= desocupado[0] + " " + desocupado[1] + " " + desocupado[2] + " " + desocupado[3] + "\n";
        frota+=ocupado[0] + " " + ocupado[1] + " " + ocupado[2] + " " + ocupado[3] ;
        arquivo.abrir("Frota.txt");
        arquivo.escrever(frota);
        arquivo.fechar();
        
        JOptionPane.showMessageDialog(null, "Concluido com sucesso!");
        new Principal().setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        Arquivo arquivo = new Arquivo();
        String frota= "";
        String[] desocupado = new String[4];
        String[] ocupado = new String[4];
        
        ocupado = arquivo.pegaOcupados();
        desocupado = arquivo.pegaDesocupados();
        
        desocupado[mcb] = Integer.toString(Integer.parseInt(desocupado[mcb])-1);
        ocupado[mcb] = Integer.toString(Integer.parseInt(ocupado[mcb])+1);
        
        frota+= desocupado[0] + " " + desocupado[1] + " " + desocupado[2] + " " + desocupado[3] + "\n";
        frota+=ocupado[0] + " " + ocupado[1] + " " + ocupado[2] + " " + ocupado[3] ;
        arquivo.abrir("Frota.txt");
        arquivo.escrever(frota);
        arquivo.fechar();
        
        JOptionPane.showMessageDialog(null, "Concluido com sucesso!");
        new Principal().setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                boolean v[] = new boolean[4];
                new Calculo(" ", " ", " ", v).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblMelhorCBCombustivel;
    private javax.swing.JLabel lblMelhorCBProposta;
    private javax.swing.JLabel lblMelhorCBTempo;
    private javax.swing.JLabel lblMelhorCBValor;
    private javax.swing.JLabel lblMelhorCustoCombustivel;
    private javax.swing.JLabel lblMelhorCustoProposta;
    private javax.swing.JLabel lblMelhorCustoTempo;
    private javax.swing.JLabel lblMelhorCustoValor;
    private javax.swing.JLabel lblMelhorTempoCombustivel;
    private javax.swing.JLabel lblMelhorTempoProposta;
    private javax.swing.JLabel lblMelhorTempoTempo;
    private javax.swing.JLabel lblMelhorTempoValor;
    // End of variables declaration//GEN-END:variables
}
